## @ingroup Input_Output-FreeCAD
# freecad_write.py
# 
# Created:  Sep 2021, Abdurrahman Gazi Yavuz


# ----------------------------------------------------------------------
#  Imports
# ----------------------------------------------------------------------
import math
import subprocess

import SUAVE
from SUAVE.Core import Units, Data

FREECADPATH = '/home/yavuzabd/squashfs-root/usr/lib/' # path to directory containing your FreeCAD.so or FreeCAD.dll file
import sys
sys.path.append(FREECADPATH)

import FreeCAD      as App
import Draft
import Part
import Import
import BOPTools.SplitFeatures
import CompoundTools.Explode

import numpy as np
import os

def write(vehicle):
    """This writes a SUAVE vehicle to FreeCAD format. It will take wing segments into account
    if they are specified in the vehicle setup file.
    
    Assumptions:
    Vehicle is composed of conventional shape fuselages, wings.

    Source:
    N/A

    Inputs:
    vehicle.
      tag                                       [-]
      wings.*.    (* is all keys)
        origin                                  [m] in all three dimensions
        spans.projected                         [m]
        chords.root                             [m]
        chords.tip                              [m]
        sweeps.quarter_chord                    [degrees]
        twists.root                             [degrees]
        twists.tip                              [degrees]
        thickness_to_chord                      [-]
        dihedral                                [degrees]
        tag                                     <string>
        Segments.*. (optional)
          twist                                 [degrees]
          percent_span_location                 [-]  .1 is 10%
          root_chord_percent                    [-]  .1 is 10%
          dihedral_outboard                     [degrees]
          sweeps.quarter_chord                  [degrees]
          thickness_to_chord                    [-]
      fuselages.fuselage (optional)
        width                                   [m]
        lengths.total                           [m]
        heights.
          maximum                               [m]
          at_quarter_length                     [m]
          at_wing_root_quarter_chord            [m]
          at_three_quarters_length              [m]
        effective_diameter                      [m]
        fineness.nose                           [-] ratio of nose section length to fuselage width
        fineness.tail                           [-] ratio of tail section length to fuselage width
        tag                                     <string>
        OpenVSP_values.  (optional)
          nose.top.angle                        [degrees]
          nose.top.strength                     [-] this determines how much the specified angle influences that shape
          nose.side.angle                       [degrees]
          nose.side.strength                    [-]
          nose.TB_Sym                           <boolean> determines if top angle is mirrored on bottom
          nose.z_pos                            [-] z position of the nose as a percentage of fuselage length (.1 is 10%)
          tail.top.angle                        [degrees]
          tail.top.strength                     [-]
          tail.z_pos (optional, 0.02 default)   [-] z position of the tail as a percentage of fuselage length (.1 is 10%)
    fuel_tank_set_index                         <int> OpenVSP object set containing the fuel tanks    

    Outputs:
    <tag>.step           This is the STEP file representation of the aircraft
    <tag>.FCStd          This is the FreeCAD file representation of the aircraft


    # Figure out if there is an airfoil provided. Otherwise this function will not work

    # Airfoils should be in Lednicer format
    # i.e. :
    #
    #EXAMPLE AIRFOIL
    # 3. 3.
    #
    # 0.0 0.0
    # 0.5 0.1
    # 1.0 0.0
    #
    # 0.0 0.0
    # 0.5 -0.1
    # 1.0 0.0

    # Note this will fail silently if airfoil is not in correct format
    # check geometry output


    Properties Used:
    N/A
    """
    # Remove old project
    if os.path.isfile(vehicle.tag + '.FCStd'):
        os.remove(vehicle.tag + '.FCStd')

    # Initialize the FreeCAD project
    App.newDocument(vehicle.tag)

    # Create Aerodynamic Surfaces
    for wing in vehicle.wings:
        create_wing(wing, vehicle.tag)

    # Create Fuselage
    for fuselage in vehicle.fuselages:
        create_freecad_fuselage(fuselage, vehicle.tag)

    # split(vehicle)

    # Export STEP file of each component
    # export_step_files(vehicle)

    # Export STEP file of water-tight geometry
    # export_one_step(vehicle)

    # Save FreeCAD file
    App.getDocument(vehicle.tag).saveAs(vehicle.tag + ".FCStd")

def create_wing(wing, vehicle_tag):
    """This write a given wing into FreeCAD format

    Assumptions:
    If wing segments are defined, they must cover the full span.
    (may work in some other cases, but functionality will not be maintained)

    Source:
    N/A

    Inputs:
    wing.
      vertical                                [Boolean]
      airfoil                                 [string]
      origin                                  [m] in all three dimensions
      spans.projected                         [m]
      chords.root                             [m]
      chords.tip                              [m]
      sweeps.leading_edge                     [degrees]
      twists.root                             [degrees]
      twists.tip                              [degrees]
      dihedral                                [degrees]
      tag                                     <string>
      Segments.*. (optional)
        twist                                 [degrees]
        percent_span_location                 [-]  .1 is 10%
        root_chord_percent                    [-]  .1 is 10%
        dihedral_outboard                     [radians]
        sweeps.quarter_chord                  [radians]
        thickness_to_chord                    [-]

    Outputs:
    area_tags                                 <dict> used to keep track of all tags needed in wetted area computation
    wing_id                                   <str>  OpenVSP ID for given wing

    Properties Used:
    N/A
    """

    # ----------------------------------------------------------------------------------------------------------------------
    # WING SPECS
    # ----------------------------------------------------------------------------------------------------------------------

    leadingEdgeSweep            = wing.sweeps.leading_edge
    halfSpan                    = wing.spans.projected / 2. / Units.millimeter
    rootChord                   = wing.chords.root / Units.millimeter
    tipChord                    = wing.chords.tip / Units.millimeter
    wingLocX                    = wing.origin[0][0] / Units.millimeter
    wingLocY                    = wing.origin[0][1] / Units.millimeter
    wingLocZ                    = wing.origin[0][2] / Units.millimeter
    dihedral                    = wing.dihedral
    airfoilFile                 = wing.airfoil

    directions                  = ['left', 'right']
    positions                   = ['upper', 'lower']
    tags                        = ['root', 'tip']

    if not wing.vertical:
        # If Segments are not defined
        if len(wing.Segments.keys())    == 0:

            airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(airfoilFile)

            for direction in directions:
                for position in positions:
                    for tag in tags:

                        if      tag     == 'root':
                            chord       = rootChord
                            xOffset     = wingLocX
                            yOffset     = wingLocY
                            zOffset     = wingLocZ

                        else:
                            chord       = tipChord
                            xOffset     = wingLocX + halfSpan * np.tan(np.radians(leadingEdgeSweep))
                            yOffset     = halfSpan
                            zOffset     = wingLocZ + halfSpan * np.tan(np.radians(dihedral))

                        if direction    == 'left':  coefficient     = -1
                        else:                       coefficient     =  1
                        if position     == 'upper': airfoil         = airfoilUpperPoints
                        else:                       airfoil         = airfoilLowerPoints

                        points                          = []

                        for point in airfoil:
                            xf = point[0] * chord + xOffset
                            yf = point[1] * chord + zOffset
                            # Save this point. I'm treating the points as x and z and setting y to 0
                            points.append(App.Vector(xf, yOffset * coefficient, yf))

                        # Create a curve and then convert to BSpline
                        curve = Part.makePolygon(points)
                        spline = Draft.makeBSpline(curve, closed=False, face=False)
                        spline.Label = '%s_%s_%s_%s' % (wing.tag, tag, direction, position)


            for direction in directions:
                for position in positions:
                    for i in range(len(tags) - 1):
                        App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_%s_%s_%s_%s_%s_%s' % (wing.tag, tags[i], direction, position, tags[i + 1], direction, position))

                        App.getDocument(vehicle_tag).ActiveObject.Sections = [App.getDocument(vehicle_tag).getObjectsByLabel('%s_root_%s_%s' % (wing.tag, direction, position))[0],
                                                                        App.getDocument(vehicle_tag).getObjectsByLabel('%s_tip_%s_%s' % (wing.tag, direction, position))[0], ]

                        App.getDocument(vehicle_tag).ActiveObject.Solid     = False
                        App.getDocument(vehicle_tag).ActiveObject.Ruled     = False
                        App.getDocument(vehicle_tag).ActiveObject.Closed    = False
                        App.getDocument(vehicle_tag).ActiveObject.Label     = '%s_%s_%s_%s_%s_%s_%s' % (wing.tag, tags[i], direction, position, tags[i + 1], direction, position)

            for direction in directions:
                    App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_%s_tip' % (wing.tag, direction))

                    App.getDocument(vehicle_tag).ActiveObject.Sections = [
                        App.getDocument(vehicle_tag).getObjectsByLabel('%s_tip_%s_lower' % (wing.tag, direction))[0],
                        App.getDocument(vehicle_tag).getObjectsByLabel('%s_tip_%s_upper' % (wing.tag, direction))[0], ]

                    App.getDocument(vehicle_tag).ActiveObject.Solid = False
                    App.getDocument(vehicle_tag).ActiveObject.Ruled = False
                    App.getDocument(vehicle_tag).ActiveObject.Closed = False
                    App.getDocument(vehicle_tag).ActiveObject.Label = '%s_%s_tip' % (wing.tag, direction)

        # Else if Segments are defined
        else:
            segmentLEOffset                 = wingLocX

            for segment in wing.Segments:

                segmentSweep                                            = segment.sweeps.leading_edge
                segmentChord                                            = segment.root_chord_percent * rootChord
                segmentSpanOffset                                       = segment.percent_span_location * halfSpan
                segmentLEOffset                                         = segmentLEOffset + segmentSpanOffset * np.tan(np.radians(segmentSweep))
                segmentAirfoil                                          = segment.airfoil
                segmentName                                             = segment.tag
                airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(segmentAirfoil)

                for direction in directions:

                    for position in positions:

                        points = []

                        if direction    == 'left':  coefficient     = -1
                        else:                       coefficient     =  1
                        if position     == 'upper': airfoil         = airfoilUpperPoints
                        else:                       airfoil         = airfoilLowerPoints

                        for point in airfoil:
                            xf = point[0] * segmentChord + segmentLEOffset
                            yf = point[1] * segmentChord
                            # Save this point. I'm treating the points as x and z and setting y to 0
                            points.append(App.Vector(xf, segmentSpanOffset * coefficient, yf))

                        # Create a curve and then convert to BSpline
                        curve = Part.makePolygon(points)
                        spline = Draft.makeBSpline(curve, closed=False, face=False)
                        spline.Label = '%s_%s_%s_%s' % (wing.tag, segmentName, direction, position)

            for i in range(len(wing.Segments.keys()) - 1):

                for direction in directions:
                    for position in positions:

                        App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_%s_%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, direction, position,  wing.Segments[i + 1].tag, direction, position))

                        App.getDocument(vehicle_tag).ActiveObject.Sections  = [App.getDocument(vehicle_tag).getObjectsByLabel('%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, direction, position))[0],
                                                                        App.getDocument(vehicle_tag).getObjectsByLabel('%s_%s_%s_%s' % (wing.tag, wing.Segments[i+1].tag, direction, position))[0]]

                        App.getDocument(vehicle_tag).ActiveObject.Solid     = False
                        App.getDocument(vehicle_tag).ActiveObject.Ruled     = False
                        App.getDocument(vehicle_tag).ActiveObject.Closed    = False

                        App.getDocument(vehicle_tag).ActiveObject.Label = '%s_%s_%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, direction, position,  wing.Segments[i + 1].tag, direction, position)

            for segment in wing.Segments:

                if segment.percent_span_location == 1.:

                    for direction in directions:

                        App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_%s_tip' % (wing.tag, direction))

                        App.getDocument(vehicle_tag).ActiveObject.Sections = [
                            App.getDocument(vehicle_tag).getObjectsByLabel(
                                '%s_%s_%s_lower' % (wing.tag, segment.tag, direction))[0],
                            App.getDocument(vehicle_tag).getObjectsByLabel(
                                '%s_%s_%s_upper' % (wing.tag, segment.tag, direction))[0], ]

                        App.getDocument(vehicle_tag).ActiveObject.Solid = False
                        App.getDocument(vehicle_tag).ActiveObject.Ruled = False
                        App.getDocument(vehicle_tag).ActiveObject.Closed = False
                        App.getDocument(vehicle_tag).ActiveObject.Label = '%s_%s_tip' % (wing.tag, direction)

    else:
        halfSpan                    = halfSpan * 2.
        # If Segments are not defined
        if len(wing.Segments.keys())    == 0:

            airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(airfoilFile)

            for position in positions:
                for tag in tags:

                    if      tag     == 'root':
                        chord       = rootChord
                        xOffset     = wingLocX
                        yOffset     = wingLocY
                        zOffset     = wingLocZ

                    else:
                        chord       = tipChord
                        xOffset     = wingLocX + halfSpan * np.tan(np.radians(leadingEdgeSweep))
                        yOffset     = halfSpan
                        zOffset     = wingLocZ + halfSpan * np.tan(np.radians(dihedral))

                    if position     == 'upper': airfoil         = airfoilUpperPoints
                    else:                       airfoil         = airfoilLowerPoints

                    points                          = []

                    for point in airfoil:
                        xf = point[0] * chord + xOffset
                        yf = point[1] * chord + zOffset
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, yf, yOffset))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = '%s_%s_%s' % (wing.tag, tag, position)

            for position in positions:
                for i in range(len(tags) - 1):
                    App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_%s_%s_%s_%s' % (wing.tag, tags[i], position, tags[i + 1], position))

                    App.getDocument(vehicle_tag).ActiveObject.Sections = [App.getDocument(vehicle_tag).getObjectsByLabel('%s_root_%s' % (wing.tag, position))[0],
                                                                    App.getDocument(vehicle_tag).getObjectsByLabel('%s_tip_%s' % (wing.tag, position))[0], ]

                    App.getDocument(vehicle_tag).ActiveObject.Solid     = False
                    App.getDocument(vehicle_tag).ActiveObject.Ruled     = False
                    App.getDocument(vehicle_tag).ActiveObject.Closed    = False
                    App.getDocument(vehicle_tag).ActiveObject.Label     = '%s_%s_%s_%s_%s' % (wing.tag, tags[i], position, tags[i + 1], position)

            for direction in directions:
                    App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_tip' % (wing.tag))

                    App.getDocument(vehicle_tag).ActiveObject.Sections = [
                        App.getDocument(vehicle_tag).getObjectsByLabel('%s_tip_lower' % (wing.tag))[0],
                        App.getDocument(vehicle_tag).getObjectsByLabel('%s_tip_upper' % (wing.tag))[0], ]

                    App.getDocument(vehicle_tag).ActiveObject.Solid = False
                    App.getDocument(vehicle_tag).ActiveObject.Ruled = False
                    App.getDocument(vehicle_tag).ActiveObject.Closed = False
                    App.getDocument(vehicle_tag).ActiveObject.Label = '%s_tip' % (wing.tag)

        # Else if Segments are defined
        else:
            segmentLEOffset                 = wingLocX

            for segment in wing.Segments:

                segmentSweep                                            = segment.sweeps.leading_edge
                segmentChord                                            = segment.root_chord_percent * rootChord
                segmentSpanOffset                                       = segment.percent_span_location * halfSpan
                segmentLEOffset                                         = segmentLEOffset + segmentSpanOffset * np.tan(np.radians(segmentSweep))
                segmentAirfoil                                          = segment.airfoil
                segmentName                                             = segment.tag
                airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(segmentAirfoil)

                for position in positions:

                    points = []

                    if position     == 'upper': airfoil         = airfoilUpperPoints
                    else:                       airfoil         = airfoilLowerPoints

                    for point in airfoil:
                        xf = point[0] * segmentChord + segmentLEOffset
                        yf = point[1] * segmentChord
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, yf, segmentSpanOffset))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = '%s_%s_%s' % (wing.tag, segmentName, position)

            for i in range(len(wing.Segments.keys()) - 1):

                for direction in directions:
                    for position in positions:

                        App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, position,  wing.Segments[i + 1].tag, position))

                        App.getDocument(vehicle_tag).ActiveObject.Sections  = [App.getDocument(vehicle_tag).getObjectsByLabel('%s_%s_%s' % (wing.tag, wing.Segments[i].tag, position))[0],
                                                                        App.getDocument(vehicle_tag).getObjectsByLabel('%s_%s_%s' % (wing.tag, wing.Segments[i+1].tag, position))[0]]

                        App.getDocument(vehicle_tag).ActiveObject.Solid     = False
                        App.getDocument(vehicle_tag).ActiveObject.Ruled     = False
                        App.getDocument(vehicle_tag).ActiveObject.Closed    = False

                        App.getDocument(vehicle_tag).ActiveObject.Label = '%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, position,  wing.Segments[i + 1].tag, position)

            for segment in wing.Segments:

                if segment.percent_span_location == 1.:

                    for direction in directions:

                        App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_tip' % (wing.tag))

                        App.getDocument(vehicle_tag).ActiveObject.Sections = [
                            App.getDocument(vehicle_tag).getObjectsByLabel(
                                '%s_%s_lower' % (wing.tag, segment.tag))[0],
                            App.getDocument(vehicle_tag).getObjectsByLabel(
                                '%s_%s_upper' % (wing.tag, segment.tag))[0], ]

                        App.getDocument(vehicle_tag).ActiveObject.Solid = False
                        App.getDocument(vehicle_tag).ActiveObject.Ruled = False
                        App.getDocument(vehicle_tag).ActiveObject.Closed = False
                        App.getDocument(vehicle_tag).ActiveObject.Label = '%s_tip' % (wing.tag)

def create_freecad_wing(wing, vehicle_tag):
    """This write a given wing into FreeCAD format
    
    Assumptions:
    If wing segments are defined, they must cover the full span.
    (may work in some other cases, but functionality will not be maintained)

    Source:
    N/A

    Inputs:
    wing.
      airfoil                                 [string]
      origin                                  [m] in all three dimensions
      spans.projected                         [m]
      chords.root                             [m]
      chords.tip                              [m]
      sweeps.leading_edge                     [degrees]
      twists.root                             [degrees]
      twists.tip                              [degrees]
      dihedral                                [degrees]
      tag                                     <string>
      Segments.*. (optional)
        twist                                 [degrees]
        percent_span_location                 [-]  .1 is 10%
        root_chord_percent                    [-]  .1 is 10%
        dihedral_outboard                     [radians]
        sweeps.quarter_chord                  [radians]
        thickness_to_chord                    [-]

    Outputs:
    area_tags                                 <dict> used to keep track of all tags needed in wetted area computation
    wing_id                                   <str>  OpenVSP ID for given wing

    Properties Used:
    N/A
    """

    # ----------------------------------------------------------------------------------------------------------------------
    # WING SPECS
    # ----------------------------------------------------------------------------------------------------------------------

    leadingEdgeSweep            = wing.sweeps.leading_edge
    halfSpan                    = wing.spans.projected / 2. / Units.millimeter
    rootChord                   = wing.chords.root / Units.millimeter
    tipChord                    = wing.chords.tip / Units.millimeter
    wingLocX                    = wing.origin[0][0] / Units.millimeter
    wingLocY                    = wing.origin[0][1] / Units.millimeter
    wingLocZ                    = wing.origin[0][2] / Units.millimeter
    dihedral                    = wing.dihedral
    airfoilFile                 = wing.airfoil

    directions                  = ['left', 'right']
    positions                   = ['upper', 'lower']
    tags                        = ['root', 'tip']
    # ----------------------------------------------------------------------------------------------------------------------
    # -------------------------------------------------MAIN WING------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------------

    # If Segments are not defined
    if len(wing.Segments.keys())    == 0:

        airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(airfoilFile)

        for direction in directions:
            for position in positions:
                for tag in tags:

                    if      tag     == 'root':
                        chord       = rootChord
                        xOffset     = wingLocX
                        yOffset     = wingLocY
                        zOffset     = wingLocZ

                    else:
                        chord       = tipChord
                        xOffset     = wingLocX + halfSpan * np.tan(np.radians(leadingEdgeSweep))
                        yOffset     = halfSpan
                        zOffset     = wingLocZ + halfSpan * np.tan(np.radians(dihedral))

                    if direction    == 'left':  coefficient     = -1
                    else:                       coefficient     =  1
                    if position     == 'upper': airfoil         = airfoilUpperPoints
                    else:                       airfoil         = airfoilLowerPoints

                    points                          = []

                    for point in airfoil:
                        xf = point[0] * chord + xOffset
                        yf = point[1] * chord + zOffset
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, yOffset * coefficient, yf))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = '%s_%s_%s' % (tag, direction, position)


        for direction in directions:
            for position in positions:
                for i in range(len(tags) - 1):
                    App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_%s_%s_%s_%s_%s' % (tags[i], direction, position, tags[i + 1], direction, position))

                    App.getDocument(vehicle_tag).ActiveObject.Sections = [App.getDocument(vehicle_tag).getObjectsByLabel('root_%s_%s' % (direction, position))[0],
                                                                    App.getDocument(vehicle_tag).getObjectsByLabel('tip_%s_%s' % (direction, position))[0], ]

                    App.getDocument(vehicle_tag).ActiveObject.Solid     = False
                    App.getDocument(vehicle_tag).ActiveObject.Ruled     = False
                    App.getDocument(vehicle_tag).ActiveObject.Closed    = False
                    App.getDocument(vehicle_tag).ActiveObject.Label     = '%s_%s_%s_%s_%s_%s' % (tags[i], direction, position, tags[i + 1], direction, position)

        for direction in directions:
                App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_tip' % (direction))

                App.getDocument(vehicle_tag).ActiveObject.Sections = [
                    App.getDocument(vehicle_tag).getObjectsByLabel('tip_%s_lower' % (direction))[0],
                    App.getDocument(vehicle_tag).getObjectsByLabel('tip_%s_upper' % (direction))[0], ]

                App.getDocument(vehicle_tag).ActiveObject.Solid = False
                App.getDocument(vehicle_tag).ActiveObject.Ruled = False
                App.getDocument(vehicle_tag).ActiveObject.Closed = False
                App.getDocument(vehicle_tag).ActiveObject.Label = '%s_tip' % (direction)

    # Else if Segments are defined
    else:
        segmentLEOffset                 = wingLocX

        for segment in wing.Segments:

            segmentSweep                                            = segment.sweeps.leading_edge
            segmentChord                                            = segment.root_chord_percent * rootChord
            segmentSpanOffset                                       = segment.percent_span_location * halfSpan
            segmentLEOffset                                         = segmentLEOffset + segmentSpanOffset * np.tan(np.radians(segmentSweep))
            segmentAirfoil                                          = segment.airfoil
            segmentName                                             = segment.tag
            airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(segmentAirfoil)

            for direction in directions:

                for position in positions:

                    points = []

                    if direction    == 'left':  coefficient     = -1
                    else:                       coefficient     =  1
                    if position     == 'upper': airfoil         = airfoilUpperPoints
                    else:                       airfoil         = airfoilLowerPoints

                    for point in airfoil:
                        xf = point[0] * segmentChord + segmentLEOffset
                        yf = point[1] * segmentChord
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, segmentSpanOffset * coefficient, yf))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = '%s_%s_%s' % (segmentName, direction, position)

        for i in range(len(wing.Segments.keys()) - 1):

            for direction in directions:
                for position in positions:

                    App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_%s_%s_%s_%s_%s' % (wing.Segments[i].tag, direction, position,  wing.Segments[i + 1].tag, direction, position))

                    App.getDocument(vehicle_tag).ActiveObject.Sections  = [App.getDocument(vehicle_tag).getObjectsByLabel('%s_%s_%s' % (wing.Segments[i].tag, direction, position))[0],
                                                                    App.getDocument(vehicle_tag).getObjectsByLabel('%s_%s_%s' % (wing.Segments[i+1].tag, direction, position))[0]]

                    App.getDocument(vehicle_tag).ActiveObject.Solid     = False
                    App.getDocument(vehicle_tag).ActiveObject.Ruled     = False
                    App.getDocument(vehicle_tag).ActiveObject.Closed    = False

                    App.getDocument(vehicle_tag).ActiveObject.Label = '%s_%s_%s_%s_%s_%s' % (wing.Segments[i].tag, direction, position,  wing.Segments[i + 1].tag, direction, position)

        for segment in wing.Segments:

            if segment.percent_span_location == 1.:

                for direction in directions:

                    App.getDocument(vehicle_tag).addObject('Part::Loft', '%s_tip' % (direction))

                    App.getDocument(vehicle_tag).ActiveObject.Sections = [
                        App.getDocument(vehicle_tag).getObjectsByLabel(
                            '%s_%s_lower' % (segment.tag, direction))[0],
                        App.getDocument(vehicle_tag).getObjectsByLabel(
                            '%s_%s_upper' % (segment.tag, direction))[0], ]

                    App.getDocument(vehicle_tag).ActiveObject.Solid = False
                    App.getDocument(vehicle_tag).ActiveObject.Ruled = False
                    App.getDocument(vehicle_tag).ActiveObject.Closed = False
                    App.getDocument(vehicle_tag).ActiveObject.Label = '%s_tip' % (direction)

def create_freecad_horizontal_tail(wing, tag):
    """This write a given horizontal tail into FreeCAD format

    Assumptions:
    If wing segments are defined, they must cover the full span.
    (may work in some other cases, but functionality will not be maintained)

    Source:
    N/A

    Inputs:
    wing.
      origin                                  [m] in all three dimensions
      spans.projected                         [m]
      chords.root                             [m]
      chords.tip                              [m]
      sweeps.leading_edge                     [degrees]
      twists.root                             [degrees]
      twists.tip                              [degrees]
      dihedral                                [degrees]
      tag                                     <string>
      Segments.*. (optional)
        twist                                 [radians]
        percent_span_location                 [-]  .1 is 10%
        root_chord_percent                    [-]  .1 is 10%
        dihedral_outboard                     [radians]
        sweeps.quarter_chord                  [radians]
        thickness_to_chord                    [-]

    Outputs:
    area_tags                                 <dict> used to keep track of all tags needed in wetted area computation
    wing_id                                   <str>  OpenVSP ID for given wing

    Properties Used:
    N/A
    """

    # Figure out if there is an airfoil provided

    # Airfoils should be in Lednicer format
    # i.e. :
    #
    # EXAMPLE AIRFOIL
    # 3. 3.
    #
    # 0.0 0.0
    # 0.5 0.1
    # 1.0 0.0
    #
    # 0.0 0.0
    # 0.5 -0.1
    # 1.0 0.0

    # Note this will fail silently if airfoil is not in correct format
    # check geometry output

    # ----------------------------------------------------------------------------------------------------------------------
    # HORIZONTAL TAIL SPECS
    # ----------------------------------------------------------------------------------------------------------------------

    hTailLESweep                = wing.sweeps.leading_edge
    hTailHalfSpan               = wing.spans.projected / 2. / Units.millimeter
    hTailRootChord              = wing.chords.root / Units.millimeter
    hTailTipChord               = wing.chords.tip / Units.millimeter
    hTailLocX                   = wing.origin[0][0] / Units.millimeter
    hTailLocY                   = wing.origin[0][1] / Units.millimeter
    hTailLocZ                   = wing.origin[0][2] / Units.millimeter
    dihedral                    = wing.dihedral
    airfoilFile                 = wing.airfoil

    # ----------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------- H - TAIL -----------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------------

    # If Segments are not defined
    if len(wing.Segments.keys())    == 0:

        airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(airfoilFile)

        Labels                                                  = ['rootHTailUpper', 'rightHTailTipUpper', 'rightHTailTipLower',
                                                                   'rootHTailLower', 'leftHTailTipLower', 'leftHTailTipUpper']
        Surfaces                                                = ['rightHTailUpper', 'rightHTailTip', 'rightHTailLower',
                                                                   'leftHTailLower', 'leftHTailTip', 'leftHTailUpper']

        for i in range(len(Labels)):

            if Labels[i][0:9] == 'rootHTail':

                if Labels[i][-5:] == 'Upper':

                    points = []

                    for point in airfoilUpperPoints:
                        xf = point[0] * hTailRootChord + hTailLocX
                        yf = point[1] * hTailRootChord
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, 0.0, yf))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

                elif Labels[i][-5:] == 'Lower':

                    points = []

                    for point in airfoilLowerPoints:
                        xf = point[0] * hTailRootChord + hTailLocX
                        yf = point[1] * hTailRootChord
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, 0.0, yf))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

            elif Labels[i][0:10] == 'rightHTail':

                if Labels[i][-5:] == 'Upper':

                    points = []

                    for point in airfoilUpperPoints:
                        xf = point[0] * hTailTipChord + hTailLocX + hTailHalfSpan * np.tan(np.radians(hTailLESweep))
                        yf = point[1] * hTailTipChord + hTailHalfSpan * np.tan(np.radians(dihedral))
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, hTailHalfSpan, yf))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

                elif Labels[i][-5:] == 'Lower':

                    points = []

                    for point in airfoilLowerPoints:
                        xf = point[0] * hTailTipChord + hTailLocX + hTailHalfSpan * np.tan(np.radians(hTailLESweep))
                        yf = point[1] * hTailTipChord + hTailHalfSpan * np.tan(np.radians(dihedral))
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, hTailHalfSpan, yf))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

            elif Labels[i][0:9] == 'leftHTail':

                if Labels[i][-5:] == 'Upper':

                    points = []

                    for point in airfoilUpperPoints:
                        xf = point[0] * hTailTipChord + hTailLocX + hTailHalfSpan * np.tan(np.radians(hTailLESweep))
                        yf = point[1] * hTailTipChord + hTailHalfSpan * np.tan(np.radians(dihedral))
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, -hTailHalfSpan, yf))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

                elif Labels[i][-5:] == 'Lower':

                    points = []

                    for point in airfoilLowerPoints:
                        xf = point[0] * hTailTipChord + hTailLocX + hTailHalfSpan * np.tan(np.radians(hTailLESweep))
                        yf = point[1] * hTailTipChord + hTailHalfSpan * np.tan(np.radians(dihedral))
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, -hTailHalfSpan, yf))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

        for i in range(len(Surfaces)):
            App.getDocument(tag).addObject('Part::Loft', Surfaces[i])

            App.getDocument(tag).ActiveObject.Sections     = [
                App.getDocument(tag).getObjectsByLabel(Labels[i])[0],
                App.getDocument(tag).getObjectsByLabel(Labels[(i + 1) % len(Labels)])[0], ]

            App.getDocument(tag).ActiveObject.Solid        = False
            App.getDocument(tag).ActiveObject.Ruled        = False
            App.getDocument(tag).ActiveObject.Closed       = False

    # Else if Segments are defined
    else:
        pass

def create_freecad_vertical_tail(wing, tag):

    """This write a given vertical tail into FreeCAD format

    Assumptions:
    If wing segments are defined, they must cover the full span.
    (may work in some other cases, but functionality will not be maintained)

    Source:
    N/A

    Inputs:
    wing.
      origin                                  [m] in all three dimensions
      spans.projected                         [m]
      chords.root                             [m]
      chords.tip                              [m]
      sweeps.leading_edge                     [radians]
      twists.root                             [radians]
      twists.tip                              [radians]
      dihedral                                [radians]
      tag                                     <string>
      Segments.*. (optional)
        twist                                 [radians]
        percent_span_location                 [-]  .1 is 10%
        root_chord_percent                    [-]  .1 is 10%
        dihedral_outboard                     [radians]
        sweeps.quarter_chord                  [radians]
        thickness_to_chord                    [-]

    Outputs:
    area_tags                                 <dict> used to keep track of all tags needed in wetted area computation
    wing_id                                   <str>  OpenVSP ID for given wing

    Properties Used:
    N/A
    """

    # Figure out if there is an airfoil provided

    # Airfoils should be in Lednicer format
    # i.e. :
    #
    # EXAMPLE AIRFOIL
    # 3. 3.
    #
    # 0.0 0.0
    # 0.5 0.1
    # 1.0 0.0
    #
    # 0.0 0.0
    # 0.5 -0.1
    # 1.0 0.0

    # Note this will fail silently if airfoil is not in correct format
    # check geometry output

    # ----------------------------------------------------------------------------------------------------------------------
    # Vertıcal TAIL SPECS
    # ----------------------------------------------------------------------------------------------------------------------

    vTailLESweep                        = wing.sweeps.leading_edge
    vTailHalfSpan                       = wing.spans.projected / Units.millimeter
    vTailRootChord                      = wing.chords.root / Units.millimeter
    vTailTipChord                       = wing.chords.tip / Units.millimeter
    vTailLocX                           = wing.origin[0][0] / Units.millimeter
    airfoilFile                         = wing.airfoil

    # ----------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------- V - TAIL -----------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------------

    # If Segments are not defined
    if len(wing.Segments.keys())    == 0:

        airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(airfoilFile)

        Labels                                                  = ['rootVTailUpper', 'tipVTailUpper', 'tipVTailLower', 'rootVTailLower']
        Surfaces                                                = ['vTailUpper', 'vTailTip', 'vTailLower', 'vTailRoot']

        for i in range(len(Labels)):

            if Labels[i][0:9] == 'rootVTail':

                if Labels[i][-5:] == 'Upper':

                    points = []

                    for point in airfoilUpperPoints:
                        xf = point[0] * vTailRootChord + vTailLocX
                        yf = point[1] * vTailRootChord
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, yf, 0.))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

                elif Labels[i][-5:] == 'Lower':

                    points = []

                    for point in airfoilLowerPoints:
                        xf = point[0] * vTailRootChord + vTailLocX
                        yf = point[1] * vTailRootChord
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, yf, 0.))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

            elif Labels[i][0:8] == 'tipVTail':

                if Labels[i][-5:] == 'Upper':

                    points = []

                    for point in airfoilUpperPoints:
                        xf = point[0] * vTailTipChord + vTailLocX + vTailHalfSpan * np.tan(np.radians(vTailLESweep))
                        yf = point[1] * vTailTipChord
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, yf, vTailHalfSpan))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

                elif Labels[i][-5:] == 'Lower':

                    points = []

                    for point in airfoilLowerPoints:
                        xf = point[0] * vTailTipChord + vTailLocX + vTailHalfSpan * np.tan(np.radians(vTailLESweep))
                        yf = point[1] * vTailTipChord
                        # Save this point. I'm treating the points as x and z and setting y to 0
                        points.append(App.Vector(xf, yf, vTailHalfSpan))

                    # Create a curve and then convert to BSpline
                    curve = Part.makePolygon(points)
                    spline = Draft.makeBSpline(curve, closed=False, face=False)
                    spline.Label = Labels[i]

        for i in range(len(Surfaces)):
            App.getDocument(tag).addObject('Part::Loft', Surfaces[i])

            App.getDocument(tag).ActiveObject.Sections = [
                App.getDocument(tag).getObjectsByLabel(Labels[i])[0],
                App.getDocument(tag).getObjectsByLabel(Labels[(i + 1) % len(Labels)])[0], ]
            App.getDocument(tag).ActiveObject.Solid = False
            App.getDocument(tag).ActiveObject.Ruled = False
            App.getDocument(tag).ActiveObject.Closed = False

    # Else if Segments are defined
    else:
        pass

def create_freecad_fuselage(fuselage, tag):
    """This write a given fuselage into FreeCAD format

    Assumptions:
    If fuselage segments are defined, they must cover the max length.
    (may work in some other cases, but functionality will not be maintained)

    Source:
    N/A

    Inputs:
    fuselage.
      origin                                  [m] in all three dimensions
      spans.projected                         [m]
      chords.root                             [m]
      chords.tip                              [m]
      sweeps.leading_edge                     [radians]
      twists.root                             [radians]
      twists.tip                              [radians]
      dihedral                                [radians]
      tag                                     <string>
      Segments.*. (optional)
        twist                                 [radians]
        percent_span_location                 [-]  .1 is 10%
        root_chord_percent                    [-]  .1 is 10%
        dihedral_outboard                     [radians]
        sweeps.quarter_chord                  [radians]
        thickness_to_chord                    [-]

    Outputs:
    area_tags                                 <dict> used to keep track of all tags needed in wetted area computation
    wing_id                                   <str>  OpenVSP ID for given wing

    Properties Used:
    N/A
    """
    # ----------------------------------------------------------------------------------------------------------------------
    # FUSELAGE SPECS
    # ----------------------------------------------------------------------------------------------------------------------

    fuselageMaxDiameter     = fuselage.effective_diameter / Units.millimeter
    fuselageLength          = fuselage.lengths.total / Units.millimeter
    totalPointsToDefine     = 50

    # ----------------------------------------------------------------------------------------------------------------------
    # CREATE FUSELAGE LINE
    # ----------------------------------------------------------------------------------------------------------------------

    yValues, xValues = searsHaackNose(fuselageMaxDiameter / 2., fuselageLength, totalPointsToDefine)

    points = []

    for x, y in zip(xValues, yValues):
        points.append(App.Vector(x, y, 0.))

    # Create a curve and then convert to BSpline
    curve = Part.makePolygon(points)
    spline = Draft.makeBSpline(curve, closed=False, face=False)
    spline.Label = 'fuselageLine'

    # ----------------------------------------------------------------------------------------------------------------------
    # CREATE FUSELAGE CROSS SECTION
    # ----------------------------------------------------------------------------------------------------------------------
    radius      = fuselageMaxDiameter / 2.
    xLocation   = fuselageLength
    points      = []
    angles      = np.linspace(0., np.pi * 2, 180)

    for angle in angles:
        points.append(App.Vector(xLocation, radius * np.sin(angle), radius * np.cos(angle)))

    points.append(App.Vector(xLocation, radius * np.sin(0.), radius * np.cos(0.)))

    # Create a curve and then convert to BSpline
    curve = Part.makePolygon(points)
    spline = Draft.makeBSpline(curve, closed=True, face=False)
    spline.Label = 'fuselageCrossSection'

    # ----------------------------------------------------------------------------------------------------------------------
    # CREATE FUSELAGE SURFACE
    # ----------------------------------------------------------------------------------------------------------------------

    App.getDocument(tag).addObject('Part::Sweep', '%s_body' % fuselage.tag)
    App.getDocument(tag).ActiveObject.Sections = [
        App.getDocument(tag).getObjectsByLabel('fuselageLine')[0], ]
    App.getDocument(tag).ActiveObject.Spine = (
    App.getDocument(tag).getObjectsByLabel('fuselageCrossSection')[0], ['Edge1', ])
    App.getDocument(tag).ActiveObject.Solid = False
    App.getDocument(tag).ActiveObject.Frenet = False

    App.ActiveDocument.recompute()

    # ----------------------------------------------------------------------------------------------------------------------
    # CREATE FUSELAGE DOWNSTREAM SURFACE
    # ----------------------------------------------------------------------------------------------------------------------

    pl = App.Placement()
    pl.Rotation.Q = (0.5, 0.5, 0.5, 0.5)
    pl.Base = App.Vector(fuselageLength, 0.0, 0.0)
    circle = Draft.makeCircle(radius=radius, placement=pl, face=False, startangle=0.0, endangle=180.0,
                              support=None)
    circle.Label = 'fuselageEndUpperArc'

    Draft.autogroup(circle)
    App.ActiveDocument.recompute()

    circle = Draft.makeCircle(radius=radius, placement=pl, face=False, startangle=180.0, endangle=360.0,
                              support=None)
    circle.Label = 'fuselageEndLowerArc'

    Draft.autogroup(circle)
    App.ActiveDocument.recompute()

    pl = App.Placement()
    pl.Rotation.Q = (0.5, 0.5, 0.5, 0.5)
    pl.Base = App.Vector(fuselageLength, fuselageMaxDiameter / 2., 0.0)
    points = [App.Vector(fuselageLength, fuselageMaxDiameter / 2., 0.0), App.Vector(fuselageLength, -fuselageMaxDiameter / 2., 0.)]

    line = Draft.makeWire(points, placement=pl, closed=False, face=False, support=None)
    line.Label = 'fuselageEndMidLine'

    Draft.autogroup(line)
    App.ActiveDocument.recompute()

    App.getDocument(tag).addObject('Part::RuledSurface', '%s_aft_1' % fuselage.tag)
    App.getDocument(tag).ActiveObject.Curve1 = (App.ActiveDocument.getObjectsByLabel('fuselageEndUpperArc')[0], ['Edge1'])
    App.getDocument(tag).ActiveObject.Curve2 = (App.ActiveDocument.getObjectsByLabel('fuselageEndMidLine')[0], ['Edge1'])
    App.getDocument(tag).recompute()

    App.getDocument(tag).addObject('Part::RuledSurface', '%s_aft_2' % fuselage.tag)
    App.getDocument(tag).ActiveObject.Curve1 = (App.ActiveDocument.getObjectsByLabel('fuselageEndLowerArc')[0], ['Edge1'])
    App.getDocument(tag).ActiveObject.Curve2 = (App.ActiveDocument.getObjectsByLabel('fuselageEndMidLine')[0], ['Edge1'])
    App.getDocument(tag).recompute()

def splitWings():

    Surfaces                                                = ['rightWingUpper', 'rightWingLower', 'leftWingLower', 'leftWingUpper']

    for i in range(len(Surfaces)):
        f                           = BOPTools.SplitFeatures.makeSlice(name='%s_Surface' % Surfaces[i])
        f.Base                      = App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0]
        f.Tools                     = [App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0], App.ActiveDocument.Fuselage][1:]
        f.Mode                      = 'Split'
        App.ActiveDocument.recompute()
        f.purgeTouched()
        input_obj                   = App.ActiveDocument.getObjectsByLabel('%s_Surface' % Surfaces[i])[0]
        Slice                       = CompoundTools.Explode.explodeCompound(input_obj)
        App.ActiveDocument.recompute()

def splitHTails():

    Surfaces                                                = ['rightHTailUpper', 'rightHTailLower', 'leftHTailLower', 'leftHTailUpper']

    for i in range(len(Surfaces)):
        f                           = BOPTools.SplitFeatures.makeSlice(name='%s_Surface' % Surfaces[i])
        f.Base                      = App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0]
        f.Tools                     = [App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0], App.ActiveDocument.Fuselage][1:]
        f.Mode                      = 'Split'
        App.ActiveDocument.recompute()
        f.purgeTouched()
        input_obj                   = App.ActiveDocument.getObjectsByLabel('%s_Surface' % Surfaces[i])[0]
        Slice                       = CompoundTools.Explode.explodeCompound(input_obj)
        App.ActiveDocument.recompute()

def splitVTails():

    Surfaces                                                = ['vTailUpper', 'vTailLower']

    for i in range(len(Surfaces)):
        f                           = BOPTools.SplitFeatures.makeSlice(name='%s_Surface' % Surfaces[i])
        f.Base              = App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0]
        f.Tools             = [App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0], App.ActiveDocument.Fuselage][1:]
        f.Mode                      = 'Split'
        App.ActiveDocument.recompute()
        f.purgeTouched()
        input_obj                   = App.ActiveDocument.getObjectsByLabel('%s_Surface' % Surfaces[i])[0]
        Slice                       = CompoundTools.Explode.explodeCompound(input_obj)
        App.ActiveDocument.recompute()

def splitFuselage():

    f                           = BOPTools.SplitFeatures.makeSlice(name='Fuselage_Surface')
    f.Base                      = App.ActiveDocument.getObjectsByLabel('Fuselage')[0]
    f.Tools                     = [App.ActiveDocument.getObjectsByLabel('Fuselage')[0], App.ActiveDocument.getObjectsByLabel('rightWingUpper')[0],
                                   App.ActiveDocument.getObjectsByLabel('rightWingLower')[0],App.ActiveDocument.getObjectsByLabel('leftWingLower')[0],
                                   App.ActiveDocument.getObjectsByLabel('leftWingUpper')[0], App.ActiveDocument.getObjectsByLabel('rightHTailUpper')[0],
                                   App.ActiveDocument.getObjectsByLabel('rightHTailLower')[0],App.ActiveDocument.getObjectsByLabel('leftHTailLower')[0],
                                   App.ActiveDocument.getObjectsByLabel('leftHTailUpper')[0],App.ActiveDocument.getObjectsByLabel('vTailUpper')[0],
                                   App.ActiveDocument.getObjectsByLabel('vTailUpper')[0]][1:]
    f.Mode = 'Split'
    App.ActiveDocument.recompute()
    f.purgeTouched()
    input_obj = App.ActiveDocument.getObjectsByLabel('Fuselage_Surface')[0]
    Slice = CompoundTools.Explode.explodeCompound(input_obj)
    App.ActiveDocument.recompute()

def split(vehicle):

    directions                      = ['left', 'right']
    positions                       = ['upper', 'lower']

    for fuselage in vehicle.fuselages:

        # SLICE WING SURFACES BY FUSELAGE

        for wing in vehicle.wings:

            if not wing.vertical:

                if len(wing.Segments.keys()) == 0:

                    for direction in directions:

                        for position in positions:

                            objectName  = '%s_root_%s_%s_tip_%s_%s' % (wing.tag, direction, position, direction, position)

                            f           = BOPTools.SplitFeatures.makeSlice(name='%s_surface' % objectName)
                            f.Base      = App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0]
                            f.Tools     = [App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0], App.getDocument(vehicle.tag).getObjectsByLabel('%s_body' % fuselage.tag)[0]][1:]
                            f.Mode      = 'Split'

                            App.ActiveDocument.recompute()
                            f.purgeTouched()

                            input_obj   = App.getDocument(vehicle.tag).getObjectsByLabel('%s_surface' % objectName)[0]

                            CompoundTools.Explode.explodeCompound(input_obj)

                            App.getDocument(vehicle.tag).recompute()

                else:

                    for direction in directions:

                        for i in range(len(wing.Segments.keys()) - 1):

                            for position in positions:

                                objectName                      = '%s_%s_%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, direction, position,  wing.Segments[i + 1].tag, direction, position)

            else:

                if len(wing.Segments.keys()) == 0:

                    for position in positions:

                        objectName  = '%s_root_%s_tip_%s' % (wing.tag, position, position)

                        f           = BOPTools.SplitFeatures.makeSlice(name='%s_surface' % objectName)
                        f.Base      = App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0]
                        f.Tools     = [App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0], App.getDocument(vehicle.tag).getObject('%s_body' % fuselage.tag)][1:]
                        f.Mode      = 'Split'

                        App.ActiveDocument.recompute()
                        f.purgeTouched()

                        input_obj   = App.getDocument(vehicle.tag).getObjectsByLabel('%s_surface' % objectName)[0]

                        CompoundTools.Explode.explodeCompound(input_obj)

                        App.getDocument(vehicle.tag).recompute()

                else:

                    for i in range(len(wing.Segments.keys()) - 1):

                        for position in positions:

                            objectName                      = '%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, position,  wing.Segments[i + 1].tag, position)

        # SLICE FUSELAGE SURFACES BY WINGS

        f       = BOPTools.SplitFeatures.makeSlice(name='%s_body_surface' % fuselage.tag)
        f.Base  = App.getDocument(vehicle.tag).getObjectsByLabel('%s_body' % fuselage.tag)[0]
        objects = []
        objects.append(App.getDocument(vehicle.tag).getObjectsByLabel('%s_body' % fuselage.tag)[0])

        for wing in vehicle.wings:

            if not wing.vertical:

                if len(wing.Segments.keys()) == 0:

                    for direction in directions:

                        for position in positions:

                            objectName  = '%s_root_%s_%s_tip_%s_%s' % (wing.tag, direction, position, direction, position)
                            objects.append(App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0])

                else:

                    for direction in directions:

                        for i in range(len(wing.Segments.keys()) - 1):

                            for position in positions:

                                objectName                      = '%s_%s_%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, direction, position,  wing.Segments[i + 1].tag, direction, position)

            else:

                if len(wing.Segments.keys()) == 0:

                    for position in positions:

                        objectName  = '%s_root_%s_tip_%s' % (wing.tag, position, position)
                        objects.append(App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0])

                else:

                    for i in range(len(wing.Segments.keys()) - 1):

                        for position in positions:

                            objectName                      = '%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, position,  wing.Segments[i + 1].tag, position)

        f.Tools = objects[1:]
        f.Mode = 'Split'

        App.ActiveDocument.recompute()
        f.purgeTouched()

        input_obj = App.getDocument(vehicle.tag).getObjectsByLabel('%s_body_surface' % fuselage.tag)[0]

        CompoundTools.Explode.explodeCompound(input_obj)

        App.getDocument(vehicle.tag).recompute()

def export_step_files(vehicle):

    directions      = ['left', 'right']
    positions       = ['upper', 'lower']

    for wing in vehicle.wings:

        if not wing.vertical:

            if len(wing.Segments.keys()) == 0:

                for direction in directions:

                    __objs__    = []
                    objectName  = '%s_%s_tip' % (wing.tag, direction)
                    fileName    = '%s_%s_tip.step' % (wing.tag, direction)

                    __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                    Import.export(__objs__, fileName)

                    for position in positions:

                        __objs__    = []
                        objectName  = '%s_root_%s_%s_tip_%s_%s' % (wing.tag, direction, position, direction, position)
                        fileName    = '%s_root_%s_%s_tip_%s_%s.step' % (wing.tag, direction, position, direction, position)

                        __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                        Import.export(__objs__, fileName)

            else:

                for direction in directions:

                    __objs__    = []
                    objectName  = '%s_%s_tip' % (wing.tag, direction)
                    fileName    = '%s_%s_tip.step' % (wing.tag, direction)

                    __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                    Import.export(__objs__, fileName)

                    for i in range(len(wing.Segments.keys()) - 1):

                        for position in positions:
                            __objs__                        = []
                            objectName                      = '%s_%s_%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, direction, position,  wing.Segments[i + 1].tag, direction, position)
                            fileName                        = '%s_%s_%s_%s_%s_%s_%s.step' % (wing.tag, wing.Segments[i].tag, direction, position,  wing.Segments[i + 1].tag, direction, position)

                            __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                            Import.export(__objs__, fileName)

        else:

            if len(wing.Segments.keys()) == 0:

                __objs__    = []
                objectName  = '%s_tip' % (wing.tag)
                fileName    = '%s_tip.step' % (wing.tag)

                __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                Import.export(__objs__, fileName)

                for position in positions:

                    __objs__    = []
                    objectName  = '%s_root_%s_tip_%s' % (wing.tag, position, position)
                    fileName    = '%s_root_%s_tip_%s.step' % (wing.tag, position, position)

                    __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                    Import.export(__objs__, fileName)

            else:

                __objs__    = []
                objectName  = '%s_tip' % (wing.tag)
                fileName    = '%s_tip.step' % (wing.tag)

                __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                Import.export(__objs__, fileName)

                for i in range(len(wing.Segments.keys()) - 1):
                    for position in positions:
                        __objs__                        = []
                        objectName                      = '%s_%s_%s_%s_%s' % (wing.tag, wing.Segments[i].tag, position,  wing.Segments[i + 1].tag, position)
                        fileName                        = '%s_%s_%s_%s_%s.step' % (wing.tag, wing.Segments[i].tag, position,  wing.Segments[i + 1].tag, position)

                        __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                        Import.export(__objs__, fileName)

    # ADD FUSELAGE SURFACES
    __objs__                                                = []
    __objs__.append(App.getDocument(vehicle.tag).getObject('%s_body' % vehicle.fuselages.fuselage.tag))
    Import.export(__objs__,'%s_body.step' % vehicle.fuselages.fuselage.tag)

    __objs__                                                = []
    __objs__.append(App.getDocument(vehicle.tag).getObject('%s_aft_1' % vehicle.fuselages.fuselage.tag))
    Import.export(__objs__,'%s_aft_1.step' % vehicle.fuselages.fuselage.tag)

    __objs__                                                = []
    __objs__.append(App.getDocument(vehicle.tag).getObject('%s_aft_2' % vehicle.fuselages.fuselage.tag))
    Import.export(__objs__,'%s_aft_2.step' % vehicle.fuselages.fuselage.tag)

    del __objs__

def export_one_step(vehicle):

    directions      = ['left', 'right']
    positions       = ['upper', 'lower']

    __objs__        = []

    for wing in vehicle.wings:

        if not wing.vertical:

            if len(wing.Segments.keys()) == 0:

                for direction in directions:

                    objectName  = '%s_%s_tip' % (wing.tag, direction)

                    __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                    for position in positions:

                        objectName  = '%s_root_%s_%s_tip_%s_%s_surface.1' % (wing.tag, direction, position, direction, position)

                        __objs__.append(App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0])

            else:

                for direction in directions:

                    objectName  = '%s_%s_tip' % (wing.tag, direction)

                    __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                    for i in range(len(wing.Segments.keys()) - 1):

                        for position in positions:

                            objectName                      = '%s_%s_%s_%s_%s_%s_%s_surface.1' % (wing.tag, wing.Segments[i].tag, direction, position,  wing.Segments[i + 1].tag, direction, position)

                            __objs__.append(App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0])

        else:

            if len(wing.Segments.keys()) == 0:

                objectName  = '%s_tip' % (wing.tag)

                __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                for position in positions:

                    objectName  = '%s_root_%s_tip_%s_surface.1' % (wing.tag, position, position)

                    __objs__.append(App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0])

            else:
                objectName  = '%s_tip' % (wing.tag)

                __objs__.append(App.getDocument(vehicle.tag).getObject(objectName))

                for i in range(len(wing.Segments.keys()) - 1):

                    for position in positions:

                        objectName                      = '%s_%s_%s_%s_%s_surface.1' % (wing.tag, wing.Segments[i].tag, position,  wing.Segments[i + 1].tag, position)

                        __objs__.append(App.getDocument(vehicle.tag).getObjectsByLabel(objectName)[0])

    # ADD FUSELAGE SURFACES

    __objs__.append(App.getDocument(vehicle.tag).getObjectsByLabel('%s_body_surface.0' % vehicle.fuselages.fuselage.tag)[0])

    __objs__.append(App.getDocument(vehicle.tag).getObjectsByLabel('%s_aft_1' % vehicle.fuselages.fuselage.tag)[0])

    __objs__.append(App.getDocument(vehicle.tag).getObjectsByLabel('%s_aft_2' % vehicle.fuselages.fuselage.tag)[0])

    Import.export(__objs__,'%s.step' % vehicle.tag)

    del __objs__

def searsHaackNose(Rmax, xMax, pointCount):

    x_dummy         = np.arange(0., 1 + 1/pointCount, 1/pointCount)
    r               = []
    x               = []
    r               = np.append(r, 0.)
    x               = np.append(x, 0.)
    rNew            = 0.
    rOld            = rNew

    for i in range(1, pointCount):

        rNew        = float(Rmax * pow((4*x_dummy[i]*(1-x_dummy[i])), 0.75))

        if rNew >= rOld:
            r           = np.append(r, rNew)

        else:
            rNew        = rOld
            r           = np.append(r, rNew)

        rOld        = rNew
        x           = np.append(x, float(xMax * x_dummy[i]))

    x               = np.append(x, xMax)
    r               = np.append(r, Rmax)

    return r, x

def read_airfoil(airfoil):

    # Airfoils should be in Lednicer format
    # i.e. :
    # Header
    # UpperSurfacePointNumber LowerSurfacePointNumber
    # Empty Row
    # UpperSurfaceXPoints UpperSurfaceYPoints
    # Empty Row
    # LowerSurfaceXPoints LowerSurfaceYPoints
    #
    #EXAMPLE AIRFOIL
    # 3. 3.
    #
    # 0.0 0.0
    # 0.5 0.1
    # 1.0 0.0
    #
    # 0.0 0.0
    # 0.5 -0.1
    # 1.0 0.0

    # ----------------------------------------------------------------------------------------------------------------------
    # READ AIRFOIL UPPER SURFACE
    # ----------------------------------------------------------------------------------------------------------------------

    upperPoints                 = []
    lowerPoints                 = []

    with open(airfoil) as f:
        # Remove the header line
        f.readline()
        # Number of points
        line                = f.readline()
        points1, points2    = line.split()
        points1             = int(float(points1))
        points2             = int(float(points2))
        # Remove the space row
        f.readline()
        # Read the upper surface
        for i in range(points1):
            line            = f.readline()
            xs, ys          = line.split()
            xf              = float(xs)
            yf              = float(ys)
            upperPoints.append([xf, yf])
        # Remove the space row
        f.readline()
        # Read the upper surface
        for i in range(points2):
            line            = f.readline()
            xs, ys          = line.split()
            xf              = float(xs)
            yf              = float(ys)
            lowerPoints.append([xf, yf])

    return upperPoints, lowerPoints
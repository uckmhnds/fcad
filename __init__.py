## @defgroup Input_Output-FreeCAD OpeFreeCADnVSP
# Functions needed to work with FreeCAD.
# @ingroup Input_Output

from .freecad_write import write
from .dummy_write import write

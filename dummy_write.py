## @ingroup Input_Output-FreeCAD
# freecad_write.py
# 
# Created:  Sep 2021, Abdurrahman Gazi Yavuz


# ----------------------------------------------------------------------
#  Imports
# ----------------------------------------------------------------------
import math

import SUAVE
from SUAVE.Core import Units, Data

FREECADPATH = '/home/yavuzabd/squashfs-root/usr/lib/' # path to directory containing your FreeCAD.so or FreeCAD.dll file
import sys
sys.path.append(FREECADPATH)

import FreeCAD      as App
import Draft
import Part
import Import
import BOPTools.SplitFeatures
import CompoundTools.Explode

import numpy as np
import os


## @ingroup Input_Output-FreeCAD
def write(vehicle):
    """This writes a SUAVE vehicle to FreeCAD format. It will take wing segments into account
    if they are specified in the vehicle setup file.
    
    Assumptions:
    Vehicle is composed of conventional shape fuselages, wings.

    Source:
    N/A

    Inputs:
    vehicle.
      tag                                       [-]
      wings.*.    (* is all keys)
        origin                                  [m] in all three dimensions
        spans.projected                         [m]
        chords.root                             [m]
        chords.tip                              [m]
        sweeps.quarter_chord                    [degrees]
        twists.root                             [degrees]
        twists.tip                              [degrees]
        thickness_to_chord                      [-]
        dihedral                                [degrees]
        tag                                     <string>
        Segments.*. (optional)
          twist                                 [degrees]
          percent_span_location                 [-]  .1 is 10%
          root_chord_percent                    [-]  .1 is 10%
          dihedral_outboard                     [degrees]
          sweeps.quarter_chord                  [degrees]
          thickness_to_chord                    [-]
      fuselages.fuselage (optional)
        width                                   [m]
        lengths.total                           [m]
        heights.
          maximum                               [m]
          at_quarter_length                     [m]
          at_wing_root_quarter_chord            [m]
          at_three_quarters_length              [m]
        effective_diameter                      [m]
        fineness.nose                           [-] ratio of nose section length to fuselage width
        fineness.tail                           [-] ratio of tail section length to fuselage width
        tag                                     <string>
        OpenVSP_values.  (optional)
          nose.top.angle                        [degrees]
          nose.top.strength                     [-] this determines how much the specified angle influences that shape
          nose.side.angle                       [degrees]
          nose.side.strength                    [-]
          nose.TB_Sym                           <boolean> determines if top angle is mirrored on bottom
          nose.z_pos                            [-] z position of the nose as a percentage of fuselage length (.1 is 10%)
          tail.top.angle                        [degrees]
          tail.top.strength                     [-]
          tail.z_pos (optional, 0.02 default)   [-] z position of the tail as a percentage of fuselage length (.1 is 10%)
    fuel_tank_set_index                         <int> OpenVSP object set containing the fuel tanks    

    Outputs:
    <tag>.step           This is the STEP file representation of the aircraft
    <tag>.FCStd          This is the FreeCAD file representation of the aircraft


    # Figure out if there is an airfoil provided. Otherwise this function will not work

    # Airfoils should be in Lednicer format
    # i.e. :
    #
    #EXAMPLE AIRFOIL
    # 3. 3.
    #
    # 0.0 0.0
    # 0.5 0.1
    # 1.0 0.0
    #
    # 0.0 0.0
    # 0.5 -0.1
    # 1.0 0.0

    # Note this will fail silently if airfoil is not in correct format
    # check geometry output


    Properties Used:
    N/A
    """



    App.newDocument(vehicle.tag)

    create_freecad_wing(vehicle.wings.main_wing, vehicle.tag)
    # create_freecad_horizontal_tail(vehicle.wings.horizontal_stabilizer, vehicle.tag)
    # create_freecad_vertical_tail(vehicle.wings.vertical_stabilizer, vehicle.tag)
    # create_freecad_fuselage(vehicle.fuselages.fuselage, vehicle.tag)
    # splitWings()
    # splitHTails()
    # splitVTails()
    # splitFuselage()
    exportStep(vehicle.tag)

    App.getDocument(vehicle.tag).saveAs(vehicle.tag + ".FCStd")

## @ingroup Input_Output-FreeCAD

def create_freecad_wing(wing, tag):
    """This write a given wing into FreeCAD format
    
    Assumptions:
    If wing segments are defined, they must cover the full span.
    (may work in some other cases, but functionality will not be maintained)

    Source:
    N/A

    Inputs:
    wing.
      airfoil                                 [string]
      origin                                  [m] in all three dimensions
      spans.projected                         [m]
      chords.root                             [m]
      chords.tip                              [m]
      sweeps.leading_edge                     [degrees]
      twists.root                             [degrees]
      twists.tip                              [degrees]
      dihedral                                [degrees]
      tag                                     <string>
      Segments.*. (optional)
        twist                                 [degrees]
        percent_span_location                 [-]  .1 is 10%
        root_chord_percent                    [-]  .1 is 10%
        dihedral_outboard                     [radians]
        sweeps.quarter_chord                  [radians]
        thickness_to_chord                    [-]

    Outputs:
    area_tags                                 <dict> used to keep track of all tags needed in wetted area computation
    wing_id                                   <str>  OpenVSP ID for given wing

    Properties Used:
    N/A
    """

    # ----------------------------------------------------------------------------------------------------------------------
    # WING SPECS
    # ----------------------------------------------------------------------------------------------------------------------

    leadingEdgeSweep            = wing.sweeps.leading_edge
    halfSpan                    = wing.spans.projected / 2. / Units.millimeter
    rootChord                   = wing.chords.root / Units.millimeter
    tipChord                    = wing.chords.tip / Units.millimeter
    wingLocX                    = wing.origin[0][0] / Units.millimeter
    wingLocY                    = wing.origin[0][1] / Units.millimeter
    wingLocZ                    = wing.origin[0][2] / Units.millimeter
    dihedral                    = wing.dihedral
    airfoilFile                 = wing.airfoil

    # ----------------------------------------------------------------------------------------------------------------------
    # -------------------------------------------------MAIN WING------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------------

    print(wing.tag)

    airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(airfoilFile)

    Labels                                                  = ['rootWingUpper', 'rightWingTipUpper', 'rightWingTipLower', 'rootWingLower', 'leftWingTipLower', 'leftWingTipUpper']
    Surfaces                                                = ['rightWingUpper', 'rightWingTip', 'rightWingLower', 'leftWingLower', 'leftWingTip', 'leftWingUpper']

    for i in range(len(Labels)):

        if Labels[i][0:8]       == 'rootWing':

            if Labels[i][-5:]      == 'Upper':

                points                          = []

                for point in airfoilUpperPoints:
                    xf              = point[0] * rootChord + wingLocX
                    yf              = point[1] * rootChord
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, 0.0, yf))

                # Create a curve and then convert to BSpline
                curve                           = Part.makePolygon(points)
                spline                          = Draft.makeBSpline(curve,closed=False,face=False)
                spline.Label                    = Labels[i]

            elif Labels[i][-5:]    == 'Lower':

                points                          = []

                for point in airfoilLowerPoints:
                    xf              = point[0] * rootChord + wingLocX
                    yf              = point[1] * rootChord
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, 0.0, yf))

                # Create a curve and then convert to BSpline
                curve                   = Part.makePolygon(points)
                spline                  = Draft.makeBSpline(curve,closed=False,face=False)
                spline.Label            = Labels[i]

        elif Labels[i][0:9]     == 'rightWing':

            if Labels[i][-5:]      == 'Upper':

                points = []

                for point in airfoilUpperPoints:

                    xf = point[0] * tipChord + wingLocX + halfSpan * np.tan(np.radians(leadingEdgeSweep))
                    yf = point[1] * tipChord + halfSpan * np.tan(np.radians(dihedral))
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, halfSpan, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

            elif Labels[i][-5:]    == 'Lower':

                points = []

                for point in airfoilLowerPoints:

                    xf = point[0] * tipChord + wingLocX + halfSpan * np.tan(np.radians(leadingEdgeSweep))
                    yf = point[1] * tipChord + halfSpan * np.tan(np.radians(dihedral))
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, halfSpan, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

        elif Labels[i][0:8]     == 'leftWing':

            if Labels[i][-5:]      == 'Upper':

                points = []

                for point in airfoilUpperPoints:

                    xf = point[0] * tipChord + wingLocX + halfSpan * np.tan(np.radians(leadingEdgeSweep))
                    yf = point[1] * tipChord + halfSpan * np.tan(np.radians(dihedral))
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, -halfSpan, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

            elif Labels[i][-5:]    == 'Lower':

                points = []

                for point in airfoilLowerPoints:

                    xf = point[0] * tipChord + wingLocX + halfSpan * np.tan(np.radians(leadingEdgeSweep))
                    yf = point[1] * tipChord + halfSpan * np.tan(np.radians(dihedral))
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, -halfSpan, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

    for i in range(len(Surfaces)):

        App.getDocument(tag).addObject('Part::Loft', Surfaces[i])

        App.getDocument(tag).ActiveObject.Sections     = [
            App.getDocument(tag).getObjectsByLabel(Labels[i])[0],
            App.getDocument(tag).getObjectsByLabel(Labels[(i+1)%len(Labels)])[0], ]

        App.getDocument(tag).ActiveObject.Solid        = False
        App.getDocument(tag).ActiveObject.Ruled        = False
        App.getDocument(tag).ActiveObject.Closed       = False
        App.getDocument(tag).ActiveObject.Label        = Surfaces[i]

def create_freecad_horizontal_tail(wing, tag):
    """This write a given horizontal tail into FreeCAD format

    Assumptions:
    If wing segments are defined, they must cover the full span.
    (may work in some other cases, but functionality will not be maintained)

    Source:
    N/A

    Inputs:
    wing.
      origin                                  [m] in all three dimensions
      spans.projected                         [m]
      chords.root                             [m]
      chords.tip                              [m]
      sweeps.leading_edge                     [degrees]
      twists.root                             [degrees]
      twists.tip                              [degrees]
      dihedral                                [degrees]
      tag                                     <string>
      Segments.*. (optional)
        twist                                 [radians]
        percent_span_location                 [-]  .1 is 10%
        root_chord_percent                    [-]  .1 is 10%
        dihedral_outboard                     [radians]
        sweeps.quarter_chord                  [radians]
        thickness_to_chord                    [-]

    Outputs:
    area_tags                                 <dict> used to keep track of all tags needed in wetted area computation
    wing_id                                   <str>  OpenVSP ID for given wing

    Properties Used:
    N/A
    """

    # Figure out if there is an airfoil provided

    # Airfoils should be in Lednicer format
    # i.e. :
    #
    # EXAMPLE AIRFOIL
    # 3. 3.
    #
    # 0.0 0.0
    # 0.5 0.1
    # 1.0 0.0
    #
    # 0.0 0.0
    # 0.5 -0.1
    # 1.0 0.0

    # Note this will fail silently if airfoil is not in correct format
    # check geometry output

    # ----------------------------------------------------------------------------------------------------------------------
    # HORIZONTAL TAIL SPECS
    # ----------------------------------------------------------------------------------------------------------------------

    hTailLESweep                = wing.sweeps.leading_edge
    hTailHalfSpan               = wing.spans.projected / 2. / Units.millimeter
    hTailRootChord              = wing.chords.root / Units.millimeter
    hTailTipChord               = wing.chords.tip / Units.millimeter
    hTailLocX                   = wing.origin[0][0] / Units.millimeter
    hTailLocY                   = wing.origin[0][1] / Units.millimeter
    hTailLocZ                   = wing.origin[0][2] / Units.millimeter
    dihedral                    = wing.dihedral
    airfoilFile                 = wing.airfoil

    # ----------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------- H - TAIL -----------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------------

    airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(airfoilFile)

    Labels                                                  = ['rootHTailUpper', 'rightHTailTipUpper', 'rightHTailTipLower',
                                                               'rootHTailLower', 'leftHTailTipLower', 'leftHTailTipUpper']
    Surfaces                                                = ['rightHTailUpper', 'rightHTailTip', 'rightHTailLower',
                                                               'leftHTailLower', 'leftHTailTip', 'leftHTailUpper']

    for i in range(len(Labels)):

        if Labels[i][0:9] == 'rootHTail':

            if Labels[i][-5:] == 'Upper':

                points = []

                for point in airfoilUpperPoints:
                    xf = point[0] * hTailRootChord + hTailLocX
                    yf = point[1] * hTailRootChord
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, 0.0, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

            elif Labels[i][-5:] == 'Lower':

                points = []

                for point in airfoilLowerPoints:
                    xf = point[0] * hTailRootChord + hTailLocX
                    yf = point[1] * hTailRootChord
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, 0.0, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

        elif Labels[i][0:10] == 'rightHTail':

            if Labels[i][-5:] == 'Upper':

                points = []

                for point in airfoilUpperPoints:
                    xf = point[0] * hTailTipChord + hTailLocX + hTailHalfSpan * np.tan(np.radians(hTailLESweep))
                    yf = point[1] * hTailTipChord + hTailHalfSpan * np.tan(np.radians(dihedral))
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, hTailHalfSpan, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

            elif Labels[i][-5:] == 'Lower':

                points = []

                for point in airfoilLowerPoints:
                    xf = point[0] * hTailTipChord + hTailLocX + hTailHalfSpan * np.tan(np.radians(hTailLESweep))
                    yf = point[1] * hTailTipChord + hTailHalfSpan * np.tan(np.radians(dihedral))
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, hTailHalfSpan, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

        elif Labels[i][0:9] == 'leftHTail':

            if Labels[i][-5:] == 'Upper':

                points = []

                for point in airfoilUpperPoints:
                    xf = point[0] * hTailTipChord + hTailLocX + hTailHalfSpan * np.tan(np.radians(hTailLESweep))
                    yf = point[1] * hTailTipChord + hTailHalfSpan * np.tan(np.radians(dihedral))
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, -hTailHalfSpan, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

            elif Labels[i][-5:] == 'Lower':

                points = []

                for point in airfoilLowerPoints:
                    xf = point[0] * hTailTipChord + hTailLocX + hTailHalfSpan * np.tan(np.radians(hTailLESweep))
                    yf = point[1] * hTailTipChord + hTailHalfSpan * np.tan(np.radians(dihedral))
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, -hTailHalfSpan, yf))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

    for i in range(len(Surfaces)):
        App.getDocument(tag).addObject('Part::Loft', Surfaces[i])

        App.getDocument(tag).ActiveObject.Sections     = [
            App.getDocument(tag).getObjectsByLabel(Labels[i])[0],
            App.getDocument(tag).getObjectsByLabel(Labels[(i + 1) % len(Labels)])[0], ]

        App.getDocument(tag).ActiveObject.Solid        = False
        App.getDocument(tag).ActiveObject.Ruled        = False
        App.getDocument(tag).ActiveObject.Closed       = False

def create_freecad_vertical_tail(wing, tag):

    """This write a given vertical tail into FreeCAD format

    Assumptions:
    If wing segments are defined, they must cover the full span.
    (may work in some other cases, but functionality will not be maintained)

    Source:
    N/A

    Inputs:
    wing.
      origin                                  [m] in all three dimensions
      spans.projected                         [m]
      chords.root                             [m]
      chords.tip                              [m]
      sweeps.leading_edge                     [radians]
      twists.root                             [radians]
      twists.tip                              [radians]
      dihedral                                [radians]
      tag                                     <string>
      Segments.*. (optional)
        twist                                 [radians]
        percent_span_location                 [-]  .1 is 10%
        root_chord_percent                    [-]  .1 is 10%
        dihedral_outboard                     [radians]
        sweeps.quarter_chord                  [radians]
        thickness_to_chord                    [-]

    Outputs:
    area_tags                                 <dict> used to keep track of all tags needed in wetted area computation
    wing_id                                   <str>  OpenVSP ID for given wing

    Properties Used:
    N/A
    """

    # Figure out if there is an airfoil provided

    # Airfoils should be in Lednicer format
    # i.e. :
    #
    # EXAMPLE AIRFOIL
    # 3. 3.
    #
    # 0.0 0.0
    # 0.5 0.1
    # 1.0 0.0
    #
    # 0.0 0.0
    # 0.5 -0.1
    # 1.0 0.0

    # Note this will fail silently if airfoil is not in correct format
    # check geometry output

    # ----------------------------------------------------------------------------------------------------------------------
    # Vertıcal TAIL SPECS
    # ----------------------------------------------------------------------------------------------------------------------

    vTailLESweep                        = wing.sweeps.leading_edge
    vTailHalfSpan                       = wing.spans.projected / Units.millimeter
    vTailRootChord                      = wing.chords.root / Units.millimeter
    vTailTipChord                       = wing.chords.tip / Units.millimeter
    vTailLocX                           = wing.origin[0][0] / Units.millimeter
    airfoilFile                         = wing.airfoil

    # ----------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------- V - TAIL -----------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------------

    airfoilUpperPoints, airfoilLowerPoints                  = read_airfoil(airfoilFile)

    Labels                                                  = ['rootVTailUpper', 'tipVTailUpper', 'tipVTailLower', 'rootVTailLower']
    Surfaces                                                = ['vTailUpper', 'vTailTip', 'vTailLower']

    for i in range(len(Labels)):

        if Labels[i][0:9] == 'rootVTail':

            if Labels[i][-5:] == 'Upper':

                points = []

                for point in airfoilUpperPoints:
                    xf = point[0] * vTailRootChord + vTailLocX
                    yf = point[1] * vTailRootChord
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, yf, 0.))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

            elif Labels[i][-5:] == 'Lower':

                points = []

                for point in airfoilLowerPoints:
                    xf = point[0] * vTailRootChord + vTailLocX
                    yf = point[1] * vTailRootChord
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, yf, 0.))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

        elif Labels[i][0:8] == 'tipVTail':

            if Labels[i][-5:] == 'Upper':

                points = []

                for point in airfoilUpperPoints:
                    xf = point[0] * vTailTipChord + vTailLocX + vTailHalfSpan * np.tan(np.radians(vTailLESweep))
                    yf = point[1] * vTailTipChord
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, yf, vTailHalfSpan))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

            elif Labels[i][-5:] == 'Lower':

                points = []

                for point in airfoilLowerPoints:
                    xf = point[0] * vTailTipChord + vTailLocX + vTailHalfSpan * np.tan(np.radians(vTailLESweep))
                    yf = point[1] * vTailTipChord
                    # Save this point. I'm treating the points as x and z and setting y to 0
                    points.append(App.Vector(xf, yf, vTailHalfSpan))

                # Create a curve and then convert to BSpline
                curve = Part.makePolygon(points)
                spline = Draft.makeBSpline(curve, closed=False, face=False)
                spline.Label = Labels[i]

    for i in range(len(Surfaces)):
        App.getDocument(tag).addObject('Part::Loft', Surfaces[i])

        App.getDocument(tag).ActiveObject.Sections = [
            App.getDocument(tag).getObjectsByLabel(Labels[i])[0],
            App.getDocument(tag).getObjectsByLabel(Labels[i + 1])[0], ]
        App.getDocument(tag).ActiveObject.Solid = False
        App.getDocument(tag).ActiveObject.Ruled = False
        App.getDocument(tag).ActiveObject.Closed = False

def create_freecad_fuselage(fuselage, tag):
    """This write a given fuselage into FreeCAD format

    Assumptions:
    If fuselage segments are defined, they must cover the max length.
    (may work in some other cases, but functionality will not be maintained)

    Source:
    N/A

    Inputs:
    fuselage.
      origin                                  [m] in all three dimensions
      spans.projected                         [m]
      chords.root                             [m]
      chords.tip                              [m]
      sweeps.leading_edge                     [radians]
      twists.root                             [radians]
      twists.tip                              [radians]
      dihedral                                [radians]
      tag                                     <string>
      Segments.*. (optional)
        twist                                 [radians]
        percent_span_location                 [-]  .1 is 10%
        root_chord_percent                    [-]  .1 is 10%
        dihedral_outboard                     [radians]
        sweeps.quarter_chord                  [radians]
        thickness_to_chord                    [-]

    Outputs:
    area_tags                                 <dict> used to keep track of all tags needed in wetted area computation
    wing_id                                   <str>  OpenVSP ID for given wing

    Properties Used:
    N/A
    """
    # ----------------------------------------------------------------------------------------------------------------------
    # FUSELAGE SPECS
    # ----------------------------------------------------------------------------------------------------------------------

    fuselageMaxDiameter     = fuselage.effective_diameter / Units.millimeter
    fuselageLength          = fuselage.lengths.total / Units.millimeter
    totalPointsToDefine     = 50

    # ----------------------------------------------------------------------------------------------------------------------
    # CREATE FUSELAGE LINE
    # ----------------------------------------------------------------------------------------------------------------------

    yValues, xValues = searsHaackNose(fuselageMaxDiameter, fuselageLength, totalPointsToDefine)

    points = []

    for x, y in zip(xValues, yValues):
        points.append(App.Vector(x, y, 0.))

    # Create a curve and then convert to BSpline
    curve = Part.makePolygon(points)
    spline = Draft.makeBSpline(curve, closed=False, face=False)
    spline.Label = 'fuselageLine'

    # ----------------------------------------------------------------------------------------------------------------------
    # CREATE FUSELAGE CROSS SECTION
    # ----------------------------------------------------------------------------------------------------------------------
    radius      = fuselageMaxDiameter
    xLocation   = 0.
    points      = []
    angles      = np.linspace(0., np.pi * 2, 180)

    for angle in angles:
        points.append(App.Vector(xLocation, radius * np.sin(angle), radius * np.cos(angle)))

    points.append(App.Vector(xLocation, radius * np.sin(0.), radius * np.cos(0.)))

    # Create a curve and then convert to BSpline
    curve = Part.makePolygon(points)
    spline = Draft.makeBSpline(curve, closed=True, face=False)
    spline.Label = 'fuselageCrossSection'

    # ----------------------------------------------------------------------------------------------------------------------
    # CREATE FUSELAGE SURFACE
    # ----------------------------------------------------------------------------------------------------------------------

    App.getDocument(tag).addObject('Part::Sweep', 'Fuselage')
    App.getDocument(tag).ActiveObject.Sections = [
        App.getDocument(tag).getObjectsByLabel('fuselageLine')[0], ]
    App.getDocument(tag).ActiveObject.Spine = (
    App.getDocument(tag).getObjectsByLabel('fuselageCrossSection')[0], ['Edge1', ])
    App.getDocument(tag).ActiveObject.Solid = False
    App.getDocument(tag).ActiveObject.Frenet = False

    App.ActiveDocument.recompute()

    # ----------------------------------------------------------------------------------------------------------------------
    # CREATE FUSELAGE DOWNSTREAM SURFACE
    # ----------------------------------------------------------------------------------------------------------------------

    pl = App.Placement()
    pl.Rotation.Q = (0.5, 0.5, 0.5, 0.5)
    pl.Base = App.Vector(fuselageLength, 0.0, 0.0)
    circle = Draft.makeCircle(radius=fuselageMaxDiameter, placement=pl, face=False, startangle=0.0, endangle=180.0,
                              support=None)
    circle.Label = 'fuselageEndUpperArc'

    Draft.autogroup(circle)
    App.ActiveDocument.recompute()

    circle = Draft.makeCircle(radius=fuselageMaxDiameter, placement=pl, face=False, startangle=180.0, endangle=360.0,
                              support=None)
    circle.Label = 'fuselageEndLowerArc'

    Draft.autogroup(circle)
    App.ActiveDocument.recompute()

    pl = App.Placement()
    pl.Rotation.Q = (0.5, 0.5, 0.5, 0.5)
    pl.Base = App.Vector(fuselageLength, fuselageMaxDiameter, 0.0)
    points = [App.Vector(fuselageLength, fuselageMaxDiameter, 0.0), App.Vector(fuselageLength, -fuselageMaxDiameter, 0.)]

    line = Draft.makeWire(points, placement=pl, closed=False, face=False, support=None)
    line.Label = 'fuselageEndMidLine'

    Draft.autogroup(line)
    App.ActiveDocument.recompute()

    App.getDocument(tag).addObject('Part::RuledSurface', 'FuselageDownstream1')
    App.getDocument(tag).ActiveObject.Curve1 = (App.ActiveDocument.getObjectsByLabel('fuselageEndUpperArc')[0], ['Edge1'])
    App.getDocument(tag).ActiveObject.Curve2 = (App.ActiveDocument.getObjectsByLabel('fuselageEndMidLine')[0], ['Edge1'])
    App.getDocument(tag).recompute()

    App.getDocument(tag).addObject('Part::RuledSurface', 'FuselageDownstream2')
    App.getDocument(tag).ActiveObject.Curve1 = (App.ActiveDocument.getObjectsByLabel('fuselageEndLowerArc')[0], ['Edge1'])
    App.getDocument(tag).ActiveObject.Curve2 = (App.ActiveDocument.getObjectsByLabel('fuselageEndMidLine')[0], ['Edge1'])
    App.getDocument(tag).recompute()

def splitWings():

    Surfaces                                                = ['rightWingUpper', 'rightWingLower', 'leftWingLower', 'leftWingUpper']

    for i in range(len(Surfaces)):
        f                           = BOPTools.SplitFeatures.makeSlice(name='%s_Surface' % Surfaces[i])
        f.Base                      = App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0]
        f.Tools                     = [App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0], App.ActiveDocument.Fuselage][1:]
        f.Mode                      = 'Split'
        App.ActiveDocument.recompute()
        f.purgeTouched()
        input_obj                   = App.ActiveDocument.getObjectsByLabel('%s_Surface' % Surfaces[i])[0]
        Slice                       = CompoundTools.Explode.explodeCompound(input_obj)
        App.ActiveDocument.recompute()

def splitHTails():

    Surfaces                                                = ['rightHTailUpper', 'rightHTailLower', 'leftHTailLower', 'leftHTailUpper']

    for i in range(len(Surfaces)):
        f                           = BOPTools.SplitFeatures.makeSlice(name='%s_Surface' % Surfaces[i])
        f.Base                      = App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0]
        f.Tools                     = [App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0], App.ActiveDocument.Fuselage][1:]
        f.Mode                      = 'Split'
        App.ActiveDocument.recompute()
        f.purgeTouched()
        input_obj                   = App.ActiveDocument.getObjectsByLabel('%s_Surface' % Surfaces[i])[0]
        Slice                       = CompoundTools.Explode.explodeCompound(input_obj)
        App.ActiveDocument.recompute()

def splitVTails():

    Surfaces                                                = ['vTailUpper', 'vTailLower']

    for i in range(len(Surfaces)):
        f                           = BOPTools.SplitFeatures.makeSlice(name='%s_Surface' % Surfaces[i])
        f.Base              = App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0]
        f.Tools             = [App.ActiveDocument.getObjectsByLabel(Surfaces[i])[0], App.ActiveDocument.Fuselage][1:]
        f.Mode                      = 'Split'
        App.ActiveDocument.recompute()
        f.purgeTouched()
        input_obj                   = App.ActiveDocument.getObjectsByLabel('%s_Surface' % Surfaces[i])[0]
        Slice                       = CompoundTools.Explode.explodeCompound(input_obj)
        App.ActiveDocument.recompute()

def splitFuselage():

    f                           = BOPTools.SplitFeatures.makeSlice(name='Fuselage_Surface')
    f.Base                      = App.ActiveDocument.getObjectsByLabel('Fuselage')[0]
    f.Tools                     = [App.ActiveDocument.getObjectsByLabel('Fuselage')[0], App.ActiveDocument.getObjectsByLabel('rightWingUpper')[0],
                                   App.ActiveDocument.getObjectsByLabel('rightWingLower')[0],App.ActiveDocument.getObjectsByLabel('leftWingLower')[0],
                                   App.ActiveDocument.getObjectsByLabel('leftWingUpper')[0], App.ActiveDocument.getObjectsByLabel('rightHTailUpper')[0],
                                   App.ActiveDocument.getObjectsByLabel('rightHTailLower')[0],App.ActiveDocument.getObjectsByLabel('leftHTailLower')[0],
                                   App.ActiveDocument.getObjectsByLabel('leftHTailUpper')[0],App.ActiveDocument.getObjectsByLabel('vTailUpper')[0],
                                   App.ActiveDocument.getObjectsByLabel('vTailUpper')[0]][1:]
    f.Mode = 'Split'
    App.ActiveDocument.recompute()
    f.purgeTouched()
    input_obj = App.ActiveDocument.getObjectsByLabel('Fuselage_Surface')[0]
    Slice = CompoundTools.Explode.explodeCompound(input_obj)
    App.ActiveDocument.recompute()

def exportStep(tag):

    __objs__                                                = []

    # ADD WING SURFACES

    Surfaces                                                = ['rightWingUpper', 'rightWingLower', 'leftWingLower', 'leftWingUpper']
    keepChilds                                              = [1, 0, 1, 0]

    for i in range(len(Surfaces)):
        __objs__.append(App.getDocument(tag).getObject('%s_Surface_child%i' % (Surfaces[i], keepChilds[i])))

    __objs__.append(App.getDocument(tag).getObject('rightWingTip'))
    __objs__.append(App.getDocument(tag).getObject('leftWingTip'))

    # ADD H TAIL SURFACES

    Surfaces                                                = ['rightHTailUpper', 'rightHTailLower', 'leftHTailLower', 'leftHTailUpper']

    for i in range(len(Surfaces)):
        __objs__.append(App.getDocument(tag).getObject('%s_Surface_child%i' % (Surfaces[i], keepChilds[i])))

    __objs__.append(App.getDocument(tag).getObject('rightHTailTip'))
    __objs__.append(App.getDocument(tag).getObject('leftHTailTip'))

    # ADD V TAIL SURFACES

    Surfaces                                                = ['vTailUpper', 'vTailLower']

    for i in range(len(Surfaces)):
        __objs__.append(App.getDocument(tag).getObject('%s_Surface_child%i' % (Surfaces[i], keepChilds[i])))

    __objs__.append(App.getDocument(tag).getObject('vTailTip'))

    # ADD FUSELAGE SURFACES

    __objs__.append(App.getDocument(tag).getObject('Fuselage_Surface_child0'))
    __objs__.append(App.getDocument(tag).getObject('FuselageDownstream1'))
    __objs__.append(App.getDocument(tag).getObject('FuselageDownstream2'))

    Import.export(__objs__, tag + '.step')

    del __objs__

def searsHaackNose(Rmax, xMax, pointCount):

    x_dummy         = np.arange(0., 1 + 1/pointCount, 1/pointCount)
    r               = []
    x               = []
    r               = np.append(r, 0.)
    x               = np.append(x, 0.)
    rNew            = 0.
    rOld            = rNew

    for i in range(1, pointCount):

        rNew        = float(Rmax * pow((4*x_dummy[i]*(1-x_dummy[i])), 0.75))

        if rNew >= rOld:
            r           = np.append(r, rNew)

        else:
            rNew        = rOld
            r           = np.append(r, rNew)

        rOld        = rNew
        x           = np.append(x, float(xMax * x_dummy[i]))

    x               = np.append(x, xMax)
    r               = np.append(r, Rmax)

    return r, x

def read_airfoil(airfoil):

    # Airfoils should be in Lednicer format
    # i.e. :
    # Header
    # UpperSurfacePointNumber LowerSurfacePointNumber
    # Empty Row
    # UpperSurfaceXPoints UpperSurfaceYPoints
    # Empty Row
    # LowerSurfaceXPoints LowerSurfaceYPoints
    #
    #EXAMPLE AIRFOIL
    # 3. 3.
    #
    # 0.0 0.0
    # 0.5 0.1
    # 1.0 0.0
    #
    # 0.0 0.0
    # 0.5 -0.1
    # 1.0 0.0

    # ----------------------------------------------------------------------------------------------------------------------
    # READ AIRFOIL UPPER SURFACE
    # ----------------------------------------------------------------------------------------------------------------------

    upperPoints                 = []
    lowerPoints                 = []

    with open(airfoil) as f:
        # Remove the header line
        f.readline()
        # Number of points
        line                = f.readline()
        points1, points2    = line.split()
        points1             = int(float(points1))
        points2             = int(float(points2))
        # Remove the space row
        f.readline()
        # Read the upper surface
        for i in range(points1):
            line            = f.readline()
            xs, ys          = line.split()
            xf              = float(xs)
            yf              = float(ys)
            upperPoints.append([xf, yf])
        # Remove the space row
        f.readline()
        # Read the upper surface
        for i in range(points2):
            line            = f.readline()
            xs, ys          = line.split()
            xf              = float(xs)
            yf              = float(ys)
            lowerPoints.append([xf, yf])

    return upperPoints, lowerPoints